function LePlayer(params) {
    var that = this;
    var userAgent = window.navigator.userAgent;
    var isAndroid = /android/i.test(userAgent); // android终端
    var isiOS = /(iPhone|iPad|iPod|iOS)/i.test(userAgent); // ios终端
    var isPC = (!isAndroid) && (!isiOS); // PC端
    var playerWrapper = $('#' + params.wrapperId);
    var clientHeight = document.documentElement.clientHeight || document.body.clientHeight;
    var clientWidth = document.documentElement.clientWidth || document.body.clientWidth;
    // 解决PC点击播放后下方空白的情况
    isPC && (clientWidth = $('.le-player').width());
    var screenHeight = window.screen.height;
    var headerHeight = screenHeight - clientHeight - 20; // 播放器头部标题高度
    // 判断手机界面有无底部系统控制条
    if(headerHeight - 5 - 44 < 5 && headerHeight - 5 - 44 > -5) {
        // 没有
    }else{
        // 有
        headerHeight = ((screenHeight - clientHeight - 20) / 5) * 3;
    }
    var urlsLength = params.urls.length; // 参数中的播放地址数量
    var currentUrl = 0; // 当前播放地址下标
    playerWrapper[0].innerHTML = '<div class="le-player">'
        + '<header class="player-header">'
        + '<span class="player-header-title">' + params.title + '</span>'
        + '</header>'
        + '<video class="le-video" preload="preload" playsinline webkit-playsinline '
        + 'x5-video-player-type="h5" x5-video-orientation="portrait" x5-video-player-fullscreen="true">'
        + '<source src=' + params.urls[currentUrl].url + '>您的浏览器暂不支持video标签'
        + '</video>'
        + '<ul class="player-controls player-clear">'
        + '<li>'
        + '<img class="play-button le-icons" src="./images/icons/play.svg" alt="">'
        + '</li>'
        + '<li class="not-live">'
        + '<img class="backward-button le-icons" src="./images/icons/backward.svg" alt="">'
        + '</li>'
        + '<li class="not-live">'
        + '<img class="forward-button le-icons" src="./images/icons/forward.svg" alt="">'
        + '</li>'
        + '<li class="time-show not-live">'
        + '<span class="show-current-time">00:00</span>'
        + '</li>'
        + '<li class="time-bar">'
        + '<p class="is-live" style="color:#fff;font-size:16px;line-height:24px;">直播中</p>'
        + '<p class="time-bar-line not-live"></p>'
        + '<p class="time-bar-lined not-live"></p>'
        + '<div class="time-bar-control not-live"></div>'
        + '</li>'
        + '<li class="time-show not-live">'
        + '<span class="show-duration">00:00</span>'
        + '</li>'
        + '<li class="set-muted">'
        + '<img class="sound-button le-icons" src="./images/icons/sound-on.svg" alt="">'
        + '</li>'
        + '<li class="screen-option">'
        + '<img class="full-screen le-icons" src="./images/icons/full-screen.svg" alt="">'
        + '<img class="video-orientation le-icons" src="./images/icons/portrait.svg" alt="">'
        + '</li>'
        + '</ul>'
        + '<img class="player-load-img" src="./images/loading.jpg" alt="">'
        + '<img class="poster-above-button" src="./images/icons/play-above.svg" alt="">'
        + '<div class="poster-wrapper"></div>'
        + '<div class="tab-tip">切换至</div>'
        + '<canvas class="print-screen-box" width="640" height="360" style="display:none;">'
        + '您的浏览器暂不支持canvas标签'
        + '</canvas>'
        + '</div>';
    params.isLive && params.urls.forEach(function (value) {
        // 根据传入的url数量生成提示标签
        $('.tab-tip')[0].innerHTML += '<span> ' + value.title + ' </span>';
    });
    var playerBox = $('.le-player'); // 播放器内层容器
    var playerHeader = $('.player-header'); // 播放器titleBar
    var playerHeaderTitle = $('.player-header-title'); // 播放器titleSlideBar
    var player = $('.le-video');
    var playerControls = $('.player-controls');
    var playerButtons = $('.play-button');
    var backwardButton = $('.backward-button');
    var forwardButton = $('.forward-button');
    var soundButton = $('.sound-button');
    var videoDuration = $('.show-duration');
    var videoCurrentTime = $('.show-current-time');
    var timeBar = $('.time-bar');
    var timeBarControl = $('.time-bar-control');
    var timeBarLined = $('.time-bar-lined');
    var currentPlayedWidth; // 播放器进度按钮当前位置距零点的距离
    var fullScreenButton = $('.full-screen');
    var videoOrientationButton = $('.video-orientation');
    var printScreenBox = $('.print-screen-box');
    var timerPlayerSlideTitle; // 播放器标题滚动字幕定时器
    var timerControls; // 播放器controlBar自动隐藏定时器
    var needGetMetaData = true; //播放时是否需要获取视频宽高
    var playerStopTimes = 0; // 播放器卡顿次数
    var timerTab; // 播放器换源提示自动显示定时器
    var posterWrapper = $('.poster-wrapper');
    var posterAboveButton = $('.poster-above-button');
    var playerLoadImage = $('.player-load-img');
    var playerIcons = $('.le-icons');
    var tabDown;
    var tabUp;
    var tabMove;
    if (isPC) {
        tabDown = 'mousedown';
        tabUp = 'mouseup';
        tabMove = 'mousemove';
    } else {
        tabDown = 'touchstart';
        tabUp = 'touchend';
        tabMove = 'touchmove';
    }
    !params.poster && (params.poster = 'https://cdn.lezhibo.com/static/portal/images/activity-default.jpg?x-oss-process=image/resize,m_fill,h_183,w_274,limit_0');
    
    // 海报样式
    posterWrapper.css({
        'position': 'absolute',
        'left': 0,
        'right': 0,
        'top': 0,
        'bottom': 0,
        'z-index': 99,
        'background': 'url(' + params.poster + ') no-repeat',
        'background-size': '100% 100%'
    });

    // 中央播放按钮
    posterAboveButton.css({
        'position': 'absolute',
        'left': '50%',
        'top': '50%',
        'width': '64px',
        'height': '64px',
        'margin-left': '-32px',
        'margin-top': '-32px',
        'z-index': 999,
        'background-color': 'rgba(0,0,0,.3)',
        'border-radius': '50%',
        'cursor': 'pointer'
    })
    .on(tabUp, function () {
        player[0].play();
    })
    .one(tabUp, function () {
        playerLoadImage.show();
    });

    // loading图初始位置
    playerLoadImage.css('top', '50%');

    // 自定义属性
    this.player = player;
    this.params = params;
    // 初始化
    this.init = function (callback) {
        var initDone = false;
        var mediaData;
        mediaData = {
            type: 'unload',
            duration: 3599,
            originalWidth: clientWidth,
            originalHeight: clientWidth * 9 / 16,
            currentWidth: clientWidth,
            currentHeight: clientWidth * 9 / 16,
            headerHeight: headerHeight
        };
        callback(mediaData);
        player.on('play', function () {
            if (!initDone) {
                var timerGetMetaData = setInterval(function () {
                    if (player[0].duration !== 0 && player[0].videoHeight > 0) {
                        clearInterval(timerGetMetaData);
                        // 加载完成后获取到video元数据
                        mediaData = {
                            type: 'loaded',
                            duration: player[0].duration,
                            originalWidth: player[0].videoWidth,
                            originalHeight: player[0].videoHeight,
                            currentWidth: player[0].customWidth,
                            currentHeight: player[0].customHeight,
                            headerHeight: headerHeight
                        };
                        callback(mediaData);
                        initDone = true;
                    } else {
                        console.log('loading...');
                    }
                }, 100);
            }
            player.on('pause',function(){
                clearInterval(timerGetMetaData);
            })
        })
    };

    // 阻止默认事件
    this.preventDefaultEvent = function (e) {
        e.preventDefault();
    };

    // 判断并设置滚动标题
    this.setPlayerSlideTitle = function () {
        var overFlowPlayerWidth = playerHeaderTitle.width() - playerHeader.width();
        if (overFlowPlayerWidth > 0) {
            var currentPlayerSlideTitleLeft = 0;
            playerHeaderTitle.css('margin-left', 0);
            timerPlayerSlideTitle = setInterval(function () {
                playerHeaderTitle.css('left', - (currentPlayerSlideTitleLeft++));
                if (currentPlayerSlideTitleLeft > overFlowPlayerWidth) {
                    setTimeout(function () {
                        currentPlayerSlideTitleLeft = 0;
                    }, 1000)
                }
            }, 30);
        } else {
            playerHeaderTitle.css({
                'left': '50%',
                'margin-left': - playerHeaderTitle.width() / 2
            });
        }
    };

    // 时间格式方法
    this.formatDate = function (time) {
        var intTime = Math.floor(time);
        var minutes = parseInt(intTime / 60);
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var seconds = intTime % 60;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        return minutes + ':' + seconds;
    };

    // 播放控制条自动消失方法
    this.playerControlsAutoHide = function(){
        playerControls.stop().fadeOut(600);
    }

    // 计算进度条位置
    var currentPosition = 0;
    var timerControlsPosition;
    this.getControlsPosition = function(){
        if(player[0].duration !== 0 && player[0].videoHeight > 0){
            currentPosition = (player[0].currentTime / player[0].duration) * timeBar.width();
            timeBarControl.css('left', currentPosition + 'px');
            timeBarLined.width(currentPosition);
        }
    }
    timeBarLined.css('background', '#1296db');

    // 阻止默认事件
    this.preventDefaultEvent = function(e) {
        e.preventDefault();
    };

    // 播放时间轴控制方法
    this.controlTimeLine = function () {
        // 进度条点击
        timeBar.on(tabUp, function (e) {
            (!isPC) && e.stopPropagation();
            clearTimeout(timerControls);
            timerControls = setTimeout(that.playerControlsAutoHide, 5000);
            clearInterval(timerControlsPosition);
            playerLoadImage.show();
            var currentTouches;
            if (isPC) {
                currentTouches = e.offsetX;
            } else {
                currentTouches = e.originalEvent.changedTouches[0].clientX - e.delegateTarget.offsetLeft;
            }
            // timeBarControl.css('left', currentTouches + 'px');
            // timeBarLined.width(currentTouches + 'px');
            player[0].currentTime = (currentTouches / timeBar.width()) * player[0].duration;
        });
        // 进度条拖拽
        if (isPC) {
            // PC
            timeBarControl.on('mousedown', function (e) {
                clearTimeout(timerControls);
                timerControls = setTimeout(that.playerControlsAutoHide, 5000);
                clearInterval(timerControlsPosition);
                var leftOffset;
                currentPlayedWidth = e.clientX - timeBarControl[0].offsetLeft;
                $(document).on('mousemove', function (e) {
                    leftOffset = e.clientX - currentPlayedWidth;
                    leftOffset = leftOffset < 0 ? 0 : ((leftOffset > timeBar[0].clientWidth - timeBarControl[0].offsetWidth)
                        ? (timeBar[0].clientWidth - timeBarControl[0].offsetWidth) : leftOffset);
                    timeBarControl.css('left', leftOffset + 'px');
                    timeBarLined.width(leftOffset + 'px');
                    player[0].currentTime = (leftOffset / timeBar.width()) * player[0].duration;
                });
                $(document).on('mouseup', function () {
                    $(document).off(tabMove + ' ' + tabUp);
                });
            });
        }else{
            // 移动
            timeBarControl[0].addEventListener("touchstart", function (e) {
                var touches = e.touches[0];
                currentPlayedWidth = touches.clientX - timeBarControl[0].offsetLeft - timeBarControl.width() / 2;
                // //阻止页面的滑动默认事件
                document.addEventListener("touchmove", that.preventDefaultEvent, false);
            }, false);
            timeBarControl[0].addEventListener("touchmove", function (e) {
                var touches = e.touches[0];
                var leftOffset = touches.clientX - currentPlayedWidth;
                leftOffset < 0 ? leftOffset = 0 : (leftOffset > timeBar[0].clientWidth - timeBarControl[0].offsetWidth
                    ? leftOffset = (timeBar[0].clientWidth - timeBarControl[0].offsetWidth) : leftOffset = leftOffset);
                timeBarControl.css('left', leftOffset);
                $('#time-bar-lined').width(leftOffset);
                player[0].currentTime = (leftOffset / timeBar.width()) * player[0].duration;
            }, false);
            timeBarControl[0].addEventListener("touchend", function (e) {
                e.stopPropagation();
                document.removeEventListener("touchmove", that.preventDefaultEvent, false);
            }, false);
        }
    };

    // 全屏方法
    this.enterFullScreen = function () {
        posterAboveButton.css('top', '50%');
        playerLoadImage.css('top', '50%');
        if (isiOS || isPC) {
            // Safari 5.0 && iOS
            player[0].webkitEnterFullScreen();
        }
        if (isAndroid) {
            fullScreenButton.hide();
            videoOrientationButton.show().attr('src', './images/icons/portrait.svg');
            player.attr('x5-video-orientation', 'landscape').css({
                'height': clientWidth,
                'width': screenHeight,
                'object-position': 'center top'
            });
            playerBox.css({
                'width': screenHeight,
                'height': clientWidth,
                'z-index': 999
            });
        }
    };

    // 截图转 base64 编码字符方法
    this.printScreen = function () {
        return printScreenBox[0].toDataURL() + '_timestamp' + that.formatDate(new Date().getTime());
    };

    // 屏幕尺寸调整
    window.onresize = function () {
        clientWidth = document.documentElement.clientWidth || document.body.clientWidth;
        playerBox.css('width', clientWidth);
        var currentPosition = (player[0].currentTime / player[0].duration) * timeBar.width();
        timeBarControl.css('left', currentPosition + 'px');
        timeBarLined.width(currentPosition + 'px');
        if (isAndroid) {
            player.css({
                'width': window.innerWidth,
                'height': window.innerHeight
            });
            playerHeader.css('width', '80%');
            clearInterval(timerPlayerSlideTitle);
            that.setPlayerSlideTitle();
        }
        isiOS && playerBox.css({
            'width': clientWidth,
            'height': (clientWidth / player[0].videoWidth) * player[0].videoHeight
        });
    };

    // 判断是否直播 && 初始化头部标题文字居中和播放器盒子高度
    if (params.isLive) {
        $('.not-live').hide();
        $('.is-live').show();
    } else {
        this.controlTimeLine();
        params.isLoop && $('.le-video').attr('loop', 'loop');
    }
    playerHeader.css({
        'height': headerHeight,
        'line-height': headerHeight * 1.1 + 'px'
    });

    // IOS取消静音设置
    if (isiOS) {
        $('.set-muted').hide();
        $('.screen-option').show();
        videoOrientationButton.hide();
    }
    if (isPC) {
        $('.screen-option').show();
        videoOrientationButton.hide();
    }

    // 腾讯X5 && 取消视频默认的点击事件
    player[0].addEventListener(tabUp, that.preventDefaultEvent, false);
    if (isAndroid) {
        player.on('x5videoenterfullscreen', function () {
            playerHeader.show();
            that.setPlayerSlideTitle();
            $('.screen-option').show();
            videoOrientationButton.hide();
            fullScreenButton.show();
            $(this).css('object-position', 'center ' + headerHeight + 'px');
            posterAboveButton.css('top', '56%');
            playerLoadImage.css('top', '56%');
            playerBox.css('z-index', 0);
            !needGetMetaData && playerBox.css('height', player[0].customHeight + headerHeight);
        })
        .on('x5videoexitfullscreen', function () {
            playerHeader.hide();
            clearInterval(timerPlayerSlideTitle);
            $(this).attr('x5-video-orientation', 'portrait').css({
                'width': window.innerWidth + 'px',
                'height': (window.innerHeight - headerHeight) + 'px',
                'object-position': 'center top'
            });
            $('.screen-option').hide();
            playerControls.stop().fadeIn(100);
            playerBox.css({
                'width': player[0].customWidth,
                'height': player[0].customHeight,
                'z-index': 0
            });
            posterAboveButton.css('top', '50%');
            playerLoadImage.css('top', '50%');
            clearInterval(timerControlsPosition);
            timerControlsPosition = setInterval(that.getControlsPosition, 200);
        });
    }

    // 控制条初始化不显示
    playerControls.hide();

    // 播放 && 暂停
    playerBox.on(tabUp, function () {
        if (!isPC) {
            if (player[0].currentTime === 0){
                playerControls.hide();
            }else{
                playerControls.toggle();
            }
        }
        clearTimeout(timerControls);
        timerControls = setTimeout(that.playerControlsAutoHide, 5000);
    });

    // PC鼠标移动显示控制条
    if (isPC) {
        playerBox.on('mousemove', function () {
            clearTimeout(timerControls);
            playerControls.stop().fadeIn(100);
            timerControls = setTimeout(that.playerControlsAutoHide, 5000);
        });
    }

    // 播放按钮点击
    playerButtons.on(tabUp, function (e) {
        e.stopPropagation();
        clearTimeout(timerControls);
        timerControls = setTimeout(that.playerControlsAutoHide, 5000);
        player[0].paused ? player[0].play() : player[0].pause();
    });

    // 快进 && 快退
    backwardButton.on(tabUp, function (e) {
        e.stopPropagation();
        clearTimeout(timerControls);
        clearInterval(timerControlsPosition);
        timerControls = setTimeout(that.playerControlsAutoHide, 5000);
        playerLoadImage.show();
        player[0].currentTime > 15 ? player[0].currentTime -= 15 : player[0].currentTime = 0;
    });
    forwardButton.on(tabUp, function (e) {
        e.stopPropagation();
        clearTimeout(timerControls);
        clearInterval(timerControlsPosition);
        timerControls = setTimeout(that.playerControlsAutoHide, 5000);
        playerLoadImage.show();
        (player[0].duration - player[0].currentTime > 15) ? (player[0].currentTime += 15) : (player[0].currentTime = player[0].duration - 2);
    });
    // 当前（总）时间显示 && 加载动画
    player.on('timeupdate', function () {
        // 显示时间
        videoCurrentTime.html(that.formatDate(this.currentTime));
        videoDuration.html(that.formatDate(player[0].duration));
        // 计算进度条位置
        var currentPosition = (this.currentTime / this.duration) * timeBar.width();
        timeBarControl.css('left', currentPosition + 'px');
        timeBarLined.width(currentPosition);
        
        // 隐藏换源提示
        params.isLive && clearTimeout(timerTab);
        playerLoadImage.hide();
    })
    .on('waiting', function () {
        // 该方法只有 IOS 支持
        isiOS && playerLoadImage.show();
    })
    .on('play', function () {
        playerButtons[0].src = './images/icons/pause.svg';
        playerControls.stop().fadeIn(100);
        // 播放开始时监听视频错误
        if (isPC && params.isLive) {
            if (/.m3u8/i.test(params.urls[currentUrl].url)){
                alert('PC端暂不支持该格式');
            }
        }
        $('.tab-tip').hide(500);
        //  增加判断防止设置多个定时器
        if (needGetMetaData && playerStopTimes === 0) {
            playerLoadImage.show();
            var timerGetMetaData = setInterval(function () {
                if (player[0].duration !== 0 && player[0].videoHeight > 0) {
                    needGetMetaData = false;
                    clearInterval(timerGetMetaData);
                    var currentMetaHeight = Math.ceil((clientWidth / player[0].videoWidth) * player[0].videoHeight);
                    player[0].customWidth = clientWidth;
                    player[0].customHeight = currentMetaHeight;
                    isAndroid && playerBox.css('height', player[0].customHeight + headerHeight);
                }else{
                    player[0].customWidth = clientWidth;
                    player[0].customHeight = 300;
                    params.isLive && playerStopTimes++;
                    if (playerStopTimes > 50 && urlsLength > 1) { $('.tab-tip').show(500); }
                }
            }, 100);
        }
        posterWrapper.hide();
        posterAboveButton.css({
            'transform': 'scale(3)',
            'opacity': '0',
            'z-index': '0',
            'transition': 'all .3s'
        });
    })
    .one('play', function () {
        isiOS && playerBox.css({
            'width': clientWidth,
            'height': (clientWidth / player[0].videoWidth) * player[0].videoHeight
        });
    })
    .on('pause', function () {
        playerButtons[0].src = './images/icons/play.svg';
        posterAboveButton.css({
            'transform': 'scale(1)',
            'opacity': '1',
            'z-index': 999,
            'transition': 'all .3s'
        });
    })
    .on('ended', function () {
        // 播放结束后回归初始状态
        playerButtons[0].src = './images/icons/play.svg';
        timeBarControl.css('left', 0);
        timeBarLined.width(0);
        $(this)[0].currentTime = 0;
    });

    // 点播中监听报错
    if (!params.isLive) {
        player.on('error',function(){
            // 错误信息
            alert('获取视频错误');
        });
    }

    // 切换线路
    $('.tab-tip span').on(tabDown, function () {
        $(this).css('color', '#1296db');
    })
    .on(tabUp, function () {
        var thatSpan = this;
        setTimeout(function () {
            $(thatSpan).css('color', '#ccc');
        }, 300);
        playerLoadImage.show();
        currentUrl = $(this).index();
        player[0].src = params.urls[currentUrl].url;
        needGetMetaData = true; // 更新播放地址后需要重新获取视频宽高以设置容器宽高
        playerStopTimes = 0; // 卡顿次数置0
        player[0].play();
    });

    // 控制时间进度
    var timeArr = [];
    var timeIndex = 0;
    var timerTimeShow = setInterval(function(){
        // 加载动画
        timeIndex++;
        timeArr.push(player[0].currentTime);
        
        if (timeIndex > 5) {
            timeIndex = 3;
            timeArr = timeArr.splice(-3,3);
        }
        if (timeArr[timeIndex-1] === timeArr[timeIndex-2]) {
            !player[0].paused && playerLoadImage.show();
        }else{
            playerLoadImage.hide();
        }
    },1000);

    // 静音
    soundButton.on(tabUp, function (e) {
        e.stopPropagation();
        clearTimeout(timerControls);
        timerControls = setTimeout(that.playerControlsAutoHide, 5000);
        player[0].muted = !player[0].muted;
        this.src = player[0].muted ? './images/icons/sound-off.svg' : './images/icons/sound-on.svg';
    });

    // 全屏操作
    fullScreenButton.on(tabUp, function (e) {
        e.stopPropagation();
        // 重置控制条自动隐藏延时器
        clearTimeout(timerControls);
        timerControls = setTimeout(that.playerControlsAutoHide, 5000);
        // 重置计算当前进度位置定时器
        clearInterval(timerControlsPosition);
        timerControlsPosition = setInterval(that.getControlsPosition, 200);
        // 全屏方法
        that.enterFullScreen();
    });

    // 横竖屏切换
    videoOrientationButton.on(tabUp, function (e) {
        e.stopPropagation();
        clearTimeout(timerControls);
        timerControls = setTimeout(that.playerControlsAutoHide, 5000);
        if (player.attr('x5-video-orientation') === 'portrait') {
            $(this).attr('src', './images/icons/portrait.svg');
            player.attr('x5-video-orientation', 'landscape').css({
                'height': clientWidth,
                'width': screenHeight
            });
            playerBox.css({
                'height': clientWidth,
                'width': screenHeight
            });
            clearInterval(timerControlsPosition);
            timerControlsPosition = setInterval(that.getControlsPosition, 200);
        } else if (player.attr('x5-video-orientation') === 'landscape') {
            $(this).attr('src', './images/icons/landscape.svg');
            player.attr('x5-video-orientation', 'portrait').css({
                'object-position': 'center center',
                'height': screenHeight,
                'width': clientWidth
            });
            playerBox.css({
                'height': screenHeight,
                'width': clientWidth
            });
            clearInterval(timerControlsPosition);
            timerControlsPosition = setInterval(that.getControlsPosition, 200);
        }
    });

    // 直播中监听网络状态或断流
    if (params.isLive) {
        var offLineTimes = 0; // 掉线次数
        var timerListenError = setInterval(function(){
            if (player[0].networkState === 3) {
                offLineTimes++;
                player[0].src = params.urls[currentUrl].url;
                player[0].load();
                player[0].play();
            }
            // 掉线次数超过20次提示可能断流
            if (offLineTimes > 20) {
                // 掉线次数置零以重新尝试获取
                offLineTimes = 0;
                // 断流轻提示
                console.log('尝试连接失败，可能存在断流。');
            }
        },500)
    }

}

// 监听播放器播放状态方法
LePlayer.prototype.listenStatus = function (eventType, callBack) {
    var player = this.player;
    var params = this.params;
    player.on('play', function () {
        eventType === 'play' && callBack();
    })
    .on('pause', function () {
        eventType === 'pause' && callBack();
    });
    // 点播中监听报错
    if (!params.isLive) {
        player.on('error', function(){
            eventType === 'error' && callBack();
        });
    }
};

// 设置播放器标题方法
LePlayer.prototype.setPlayerTitle = function (title) {
    $('.player-header-title').html(title);
};

// 设置当前播放位置
LePlayer.prototype.currentTime = function(time){
    if(time){
        (this.player)[0].currentTime = time;
        (this.player)[0].play();
    }else{
        return (this.player)[0].currentTime;
    }
}